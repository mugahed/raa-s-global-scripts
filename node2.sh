#!/bin/bash

set -ex

# Check if at least one argument is provided
if [ $# -eq 0 ]; then
  echo "Error: Please provide the remote host IP."
  exit 1
fi

# Assign the first argument to a variable named "var"
remote_host="$1"

~/raa-s-global-scripts/bin/deploy-open5gs.sh 
~/raa-s-global-scripts/bin/expand_pub_ip_node2.sh $remote_host 
sudo cp ~/raa-s-global-scripts/etc/node2/* /var/tmp/open5gs/configs/open5gs/ 
sudo ~/raa-s-global-scripts/bin/edit_hosts_node2.sh 
#~/raa-s-global-scripts/bin/install_PacketRusher.sh 
~/raa-s-global-scripts/bin/run_core_tmux_node2.sh 
sudo ~/raa-s-global-scripts/bin/install-user-node2.sh 

