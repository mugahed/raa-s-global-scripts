#!/bin/bash

set -ex

PUB_IP=$(dig @208.67.222.220 myip.opendns.com +short)


find  ~/raa-s-global-scripts ! -name 'expand_pub_ip*' -type f -exec sed -i "s/node1_pub_IP/$PUB_IP/g" {} \;

