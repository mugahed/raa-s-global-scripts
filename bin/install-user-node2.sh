#!/bin/bash

set -ex


wget https://raw.githubusercontent.com/open5gs/open5gs/main/misc/db/open5gs-dbctl
chmod +x open5gs-dbctl
./open5gs-dbctl add_ue_with_apn 001010000000001 00112233445566778899aabbccddeeff 63BFA50EE6523365FF14C1F45F88737D internet  # IMSI,K,OPC
./open5gs-dbctl type 001010000000001 1  # APN type IPV4

ip netns add ue1

