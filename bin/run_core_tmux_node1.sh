#!/bin/bash

set -ex

cd /var/tmp/open5gs

tmux new-session -d -s my_session
tmux split-window -t my_session:0 -v
tmux split-window -t my_session:0 -v

tmux new-window -t my_session  # Create second window
tmux split-window -t my_session:1 -v
tmux split-window -t my_session:1 -v

tmux new-window -t my_session  # Create third window
tmux split-window -t my_session:2 -v
tmux split-window -t my_session:2 -v

tmux new-window -t my_session  # Create forth window
tmux split-window -t my_session:3 -v
tmux split-window -t my_session:3 -v

tmux new-window -t my_session  # Create fifth window
tmux split-window -t my_session:4 -v

tmux send-keys -t "my_session:0.0" 'sudo ./install/bin/open5gs-nrfd -c /var/tmp/open5gs/configs/open5gs/nrf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:0.1" 'sudo ./install/bin/open5gs-scpd -c /var/tmp/open5gs/configs/open5gs/scp.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:0.2" 'sudo ./install/bin/open5gs-seppd -c /var/tmp/open5gs/configs/open5gs/sepp.yaml' Enter '  ' Enter

tmux send-keys -t "my_session:1.0" 'sudo ./install/bin/open5gs-amfd -c /var/tmp/open5gs/configs/open5gs/amf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:1.1" 'sudo ./install/bin/open5gs-smfd -c /var/tmp/open5gs/configs/open5gs/smf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:1.2" 'sudo ./install/bin/open5gs-upfd -c /var/tmp/open5gs/configs/open5gs/upf.yaml.in' Enter '  ' Enter

tmux send-keys -t "my_session:2.0" 'sudo ./install/bin/open5gs-ausfd -c /var/tmp/open5gs/configs/open5gs/ausf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:2.1" 'sudo ./install/bin/open5gs-udmd -c /var/tmp/open5gs/configs/open5gs/udm.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:2.2" 'sudo ./install/bin/open5gs-pcfd -c /var/tmp/open5gs/configs/open5gs/pcf.yaml.in' Enter '  ' Enter

tmux send-keys -t "my_session:3.0" 'sudo ./install/bin/open5gs-nssfd -c /var/tmp/open5gs/configs/open5gs/nssf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:3.1" 'sudo ./install/bin/open5gs-bsfd -c /var/tmp/open5gs/configs/open5gs/bsf.yaml.in' Enter '  ' Enter
tmux send-keys -t "my_session:3.2" 'sudo ./install/bin/open5gs-udrd -c /var/tmp/open5gs/configs/open5gs/udr.yaml.in' Enter '  ' Enter

#tmux send-keys -t "my_session:4.0" 'sudo ~/PacketRusher/packetrusher --config ~/raa-s-global-scripts/etc/packetRusher/node1_config.yml ue' Enter '  ' Enter

