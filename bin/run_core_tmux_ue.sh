#!/bin/bash

set -ex


tmux new-session -d -s my_session
tmux split-window -t my_session:0 -v
tmux split-window -t my_session:0 -v

tmux send-keys -t "my_session:0.0" 'sudo ~/PacketRusher/packetrusher --config ~/raa-s-global-scripts/etc/packetRusher/node1_config.yml ue' Enter '  ' Enter

