#!/bin/bash

set -ex

# Assign the first argument to a variable named "var"
remote_host="$1"

PUB_IP=$(dig @208.67.222.220 myip.opendns.com +short)


find  ~/raa-s-global-scripts ! -name 'expand_pub_ip*' -type f -exec sed -i "s/node1_pub_IP/$PUB_IP/g" {} \;

find  ~/raa-s-global-scripts ! -name 'expand_pub_ip*' -type f -exec sed -i "s/node2_pub_remote_IP/$remote_host/g" {} \;
