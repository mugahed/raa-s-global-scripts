#!/bin/bash

set -ex

sudo su -c "echo '     
127.0.0.10      nrf.5gc.mnc070.mcc999.3gppnetwork.org
127.0.0.11      ausf.5gc.mnc070.mcc999.3gppnetwork.org
127.0.0.12      udm.5gc.mnc070.mcc999.3gppnetwork.org
' >> /etc/hosts  
" root

