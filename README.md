# Introduction

This is the artifact work for RaaS paper, the easiest way to run the experiment is to use [Powder wireless](https://powderwireless.net/), and [this](https://www.powderwireless.net/show-profile.php?uuid=abed6228-cad1-11ee-9f39-e4434b2381fc) experiment. however in case you want to run the experiment in your own servers, you prefere specific geograghical locations, or you are not a powder user. use this repo.

## Requirments

The installation script installs most of the missing requiremetns, however the used CPU should support Advanced Vector Extension(AVX2).

- Ubuntu 22.04 or newer.
- tmux (usually installed by default).

After the installation scripts are over we will use tmux to move between core network nodes and collects packet capture. a little knowledge about tmux is required.

## Installtion

Once servers is ready clone this reop on the home dirctory of a non-root users. on the first server run:

```bash
./node1 NODE2_IP_ADDR
```

And on the second server run

```bash
./node2 NODE1_IP_ADDR
```

This installtion on each node will take about 15 minutes.

## tmux attach

After the installtion is done on both nodes log into the the active tmux session "my_session" using:

```bash
tmux attach -t my_session
```

tmux windows/panels sturcture:

- Window 0
  - "my_session:0.0" nrf
  - "my_session:0.1" scp
  - "my_session:0.2" sepp
- Window 1
  - "my_session:1.0" amf
  - "my_session:1.1" smf
  - "my_session:1.2" upf
- Window 2  
  - "my_session:2.0" ausf
  - "my_session:2.1" udm
  - "my_session:2.2" pcf
- Window 3
  - "my_session:3.0" nssf
  - "my_session:3.1" bsf
  - "my_session:3.2" udr
- Window 4
  - "my_session:4.0" UE

## Running tests

To run the test go the UE panel and run:

```bash
sudo ~/PacketRusher/packetrusher --config ~/raa-s-global-scripts/etc/packetRusher/node1_config.yml ue
```

Tshark Command:

```
tshark -l -i en0 -i llw0 -i awdl0 -w raas_v1.pcap
```

Iperf3 commands:
Server

```
sudo iperf3 -s -p 11225
```

Client"UE":

```
sudo ip vrf exec vrf0000000001 iperf3 -c IPERF_SERVER -p PORT -t 9000
```



NOTE: update nodex.sh scripts to install tshark & iperf3